"""
This file contains generic utility functions
"""
import os
import re
from random import choice
import shutil
import socket
import validators
import string
import hashlib
from copy import deepcopy
from json import load as json_loader, JSONDecodeError
import time
from datetime import datetime

from xpresso.ai.core.commons.utils.constants import ENV_XPRESSO_PACKAGE_PATH, \
    DEFAULT_XPRESSO_PACKAGE_PATH, \
    DEFAULT_XPRESSO_CONFIG_PATH, ENV_XPRESSO_CONFIG_PATH, \
    PASSWORD_ENCRYPTION_KEY
from xpresso.ai.core.commons.exceptions.xpr_exceptions \
    import FieldTypeException, FileExistsException, FileNotFoundException


def get_base_pkg_location():
    """ Gets the xpresso base package location from the environment variable.
    If environment variable is not found, then return default value

    Returns:
        str: xpresso base package location
    """
    return os.environ.get(ENV_XPRESSO_PACKAGE_PATH,
                          DEFAULT_XPRESSO_PACKAGE_PATH)


def get_default_config_path():
    """ Gets the default config path location using environment variable.
    If no environment variable is present, then return default config

    Returns:
        str: config_path
    """
    return os.environ.get(ENV_XPRESSO_CONFIG_PATH,
                          DEFAULT_XPRESSO_CONFIG_PATH)


def get_version():
    """
    Fetches client and api_server versions and prints out in the stdout

    Returns:
        str: version string of the project
    """
    try:
        version_file_name = os.path.join(get_base_pkg_location(), 'VERSION')
        version_fs = open(version_file_name)
        version = version_fs.read().strip()
        version_fs.close()
    except FileNotFoundError:
        # Using default version
        version = '-1'
    return version


def check_if_valid_ipv4_address(ip_address: str) -> bool:
    """ Checks if the ip address is valid IPv4 address"""
    is_valid = validators.ipv4(ip_address)
    if is_valid:
        return True
    return False


def check_if_valid_ipv6_address(ip_address: str) -> bool:
    """ Checks if the ip address is valid IPv6 address"""
    is_valid = validators.ipv6(ip_address)
    if is_valid:
        return True
    return False


def check_if_valid_dns_name(dns_name) -> bool:
    """ Checks if the dns name (fqdn) is valid """
    dns_name = dns_name.strip()
    is_valid = validators.domain(dns_name)
    if is_valid or dns_name == 'localhost':
        return True
    return False


def str2bool(input_string: str):
    """ Convert string to bool  value
    Args:
        input_string(str): string to convert to bool
    Returns:
        bool: True, if string is either true/True else False
    """
    if not input_string:
        return False
    true_set = {"true", "True"}
    return input_string.lower() in true_set


def encrypt_string(pwd: str) -> str:
    """ Encrypt string
    Args:
        pwd(str): string needed to be encrypted

    Returns:
        str: encrypted string
    """
    from cryptography.fernet import Fernet  # This is intentional

    cipher_suite = Fernet(PASSWORD_ENCRYPTION_KEY)
    encoded_text = cipher_suite.encrypt(pwd.encode("utf-8"))
    return encoded_text.decode("utf-8")


def decrypt_string(pwd: str) -> str:
    """ Decrypt string. Only those string which are envrypter using
    encrypt_string function can only be decrypted. In case of error, return
    existing pwd
    Args:
        pwd(str): string needed to be encrypted
    Returns:
        str: decrypted string
    """
    import cryptography
    from cryptography.fernet import Fernet  # This is intentional
    try:

        cipher_suite = Fernet(PASSWORD_ENCRYPTION_KEY)
        encoded_text = cipher_suite.decrypt(pwd.encode("utf-8"))
        return encoded_text.decode("utf-8")
    except (cryptography.fernet.InvalidSignature,
            cryptography.fernet.InvalidToken):
        return pwd


def convert_to_title(base_str: str) -> str:
    """ Convert the string into a title"""
    return re.sub("[^A-Za-z0-9]+", '', base_str.title())


def move_file(source, destination, overwrite=False):
    """
    Move a file from source path to destination path
    Args:
        source: Path to the source file
        destination: Path to the destination
        overwrite: Overwrites file in destination if True
    Returns:
        True if file moved successfully
    """
    if overwrite:
        file_name = os.path.basename(source)
        destination_file = os.path.join(destination, file_name)
        if os.path.exists(destination_file):
            os.remove(destination_file)
    if not os.path.exists(source):
        print("Source path missing")
        return False
    try:
        shutil.move(source, destination)
        return True
    except OSError:
        print("File exists in destination path")
        return False


def remove_keys_from_dict(dictionary: dict, key_list: list):
    """
    Removes list of keys from the dictionary
    Args:
        dictionary:  target dictionary
        key_list: list of keys to delete
    """
    for key in key_list:
        if key in dictionary:
            dictionary.pop(key)


def remove_keys_from_list(target_list: list, key_list: list):
    """
    Removes list of keys from the list
    Args:
        target_list:  target list
        key_list: list of keys to delete
    """
    for key in key_list:
        if key in target_list:
            target_list.remove(key)


def resolve_dns_name(dns_name):
    try:
        return socket.gethostbyname(dns_name)
    except socket.gaierror:
        return None
    except Exception:
        return None


def get_int_value(key, val):
    """
    typecasts a variable to integer and returns its value

    Args:
        key: key of the variable that needs to be type casted
        val: value of variable that needs to be type casted
    """
    try:
        return int(val)
    except (TypeError, ValueError):
        # occurs if provided limit value is invalid
        raise FieldTypeException(field=key, valid_type="int")


def validate_memory(value):
    """
    validates if the input value is valid memory specification
    m and M signifies megabytes
    g and G signifies gigabytes
    Accepted formats - XXm, XXM, XXg, XXG where XX should be an integer
    """
    if not isinstance(value, str):
        raise ValueError
    if not value[-1] in 'mMgG':
        raise ValueError('Invalid format, must end with mMgG')
    numeric_value = value[:-1]
    try:
        numeric_value = int(numeric_value)
    except ValueError:
        raise ValueError('Input not a number')
    return f'{numeric_value}{value[-1]}'


def validate_numeric(value):
    try:
        return int(value)
    except ValueError:
        raise ValueError


def str_hash256(string_input: str):
    """
    generates sha256 for input string

    Args:
        string_input: input string
    Returns:
        sha256
    """

    return hashlib.sha256(string_input.encode()).hexdigest()


def extract_json_from_file(json_path: str) -> dict:
    """
    extract contents of json from file at json_path

    Args:
        json_path: path to the json file
    Returns:
     returns a dictionary with contents of json file
    """
    try:
        with open(json_path, "r") as json_data:
            data = json_loader(json_data)
            return data
    except JSONDecodeError:
        return dict()


def generate_random_string(string_length: int = 9) -> str:
    """
    generates a random string

    Returns:
        returns random string
    """
    string_letter = string.ascii_letters
    random_string = ''.join(
        choice(string_letter) for i in range(string_length)
    )
    return random_string


def create_folder(folder_path, overwrite_folder: bool = False):
    """
    creates a folder locally at provided path

    Args:
        folder_path: path where folder needs to be created
        overwrite_folder: flag to specify
    """
    default_mode = 0o777
    try:
        os.makedirs(folder_path, default_mode, overwrite_folder)
    except (FileExistsError, OSError):
        raise FileExistsException(folder_path)


def copy_contents_between_folders(from_path, to_path, overwrite=False):
    """
    copy contents of one folder into another
    Args:
        from_path: location from which content is copied
        to_path: location to which content is copied
        overwrite: flag to check if overwriting content is allowed
    """
    pass


def move_directory(source, destination):
    """Move directory from source to destination. Overwrites
        if exists"""
    destination_folder = os.path.join(destination, os.path.basename(source))
    if os.path.exists(destination_folder):
        print(f"Overwriting {destination_folder}")
        os.system(f"rm -rf {destination_folder}")
    try:
        shutil.move(source, destination)
        return True
    except OSError:
        print("File exists in destination path")
        return False


def get_file_hierarchy(base_path: str, show_hidden: bool = False,
                       exclude_list: list = []):
    """
    fetch the file hierarchy at given path on local system

    Args:
        base_path: path at which the hierarchy of the files needs to fetched
        show_hidden: flag to specify whether to return hidden files in output
        exclude_list: list of files or folders that needs to be excluded
    Returns:
        returns the hierarchy of files as a generator or list
    """
    if not os.path.exists(base_path):
        raise FileNotFoundException(
            f"Unable to find any files at {base_path}"
        )
    file_dir = {
        "parent": str(),
        "path": str(),
        "sub_dirs": list(),
        "files": list()
    }
    files_list = list()
    hidden_files_prefixes = ('_', '.')
    for dir_path, subdir_list, files in os.walk(base_path):
        subdir_list[:] = [dir_name for dir_name in subdir_list
                          if not dir_name.startswith(hidden_files_prefixes)]
        tmp_copy = deepcopy(file_dir)
        tmp_copy["path"] = dir_path
        tmp_copy["parent"] = os.path.basename(dir_path)
        tmp_copy["sub_dirs"] = get_file_attributes(dir_path, subdir_list)
        tmp_copy["files"] = get_file_attributes(dir_path, files)
        files_list.append(tmp_copy)
    return files_list


def get_file_attributes(base_path: str, list_of_file_items: list) -> list:
    """
    get all the file attributes info as a list
    fetch the file info for each file in list_of_file_items

    Args:
         base_path: base_path i.e. common for all files in the list
         list_of_file_items: list of file item names
    Returns:
        returns a list of dictionaries which contain the file attributes
        name, size and last modified time
    """
    items_list = []
    for file_item in list_of_file_items:
        file_path = os.path.join(base_path, file_item)
        temp_file_object = get_file_info_as_dict(file_path)
        items_list.append(temp_file_object)
    return items_list


def get_file_info_as_dict(file_path: string):
    """
    fetch the info of the file as a dictionary using
    os library path & stat methods

    Args:
        file_path: path of the file on local system
    Returns:
        returns the file info as a dictionary.
        Includes name, size and last modified time
    """
    file_stat = os.stat(file_path)
    file_object = {
        "name": os.path.basename(file_path),
        "size": get_size_in_readable_format(file_stat.st_size),
        "mtime": time.ctime(file_stat.st_mtime)
    }
    return file_object


def get_size_in_readable_format(size_in_bytes):
    """

    1KiB = 1024 bytes
    1MiB = 1048576 bytes
    1GiB = 1073741824 bytes
    :param size_in_bytes:
    :return:
    """
    kibibyte = 1024
    mebibyte = 1048576
    gibibyte = 1073741824
    byte_size = int(size_in_bytes)
    out_size = ""
    if byte_size < kibibyte:
        out_size = str(byte_size) + "B"
    elif byte_size < mebibyte:
        out_size = str(round(byte_size / 1024)) + "KiB"
    elif byte_size < gibibyte:
        out_size = str(round(byte_size / 1048576)) + "MiB"
    else:
        out_size = str(round(byte_size / 1073741824)) + "GiB"
    return out_size


def get_time_difference(start_datetime, end_datetime):
    """ Find the difference between  end_datetime and start_datetime
    Returns:
         tuple: (hours, minutes, seconds)
    """
    seconds_in_day = 24 * 60 * 60
    seconds_in_hours = 60 * 60
    difference = end_datetime - start_datetime
    total_diff_in_seconds = difference.days * seconds_in_day + difference.seconds
    hours = int(total_diff_in_seconds / seconds_in_hours)
    minutes = int((total_diff_in_seconds % seconds_in_hours) / 60)
    seconds = int((total_diff_in_seconds % seconds_in_hours) % 60)
    return hours, minutes, seconds


def read_file_obj_in_chunks(file_object, chunk_size=1024 * 1024):
    """Reads  file in chunks. Default chunk size: 1M.

    Args:
        file_object(file): file stream object
        chunk_size(int): size of the chunk to read at a time
    Returns:
        byte: iterable byte data
    """
    while True:
        try:
            chunk_data = file_object.read(chunk_size)
            if not chunk_data:
                break
            yield chunk_data
        except (FileNotFoundError, OSError,
                AttributeError, TypeError, UnicodeDecodeError):
            break


def backup_folder(path_to_folder: str, clear_data: bool = False):
    """
    backup a folder by copying all of its contents to another folder
    which is created at the same location as source

    Args:
        path_to_folder: path of the folder to backup
        clear_data: A boolean flag to decide whether the current directory
            needs to be emptied after a backup
    Returns:
        returns true in case of successful backup
    """
    # check if the provided folder exists or not
    if not os.path.exists(path_to_folder):
        raise FileNotFoundException(
            f"No folder found at this path `{path_to_folder}` to backup")
    # If the file type of the item at the path is not a folder. Abort backup
    if not os.path.isdir(path_to_folder):
        raise FileExistsException("No folder found to backup at this path")
    # Name of the backup folder is generated by adding
    # current time to original folder name
    current_time = datetime.now().strftime("%Y_%m_%d__%T")
    backup_folder_path = f"{path_to_folder}.backup__{current_time}"
    # copytree/move suggests destination folder to not be created.
    if clear_data:
        # For clearing out the data, we backup by moving the data and
        # then creating the current directory again
        shutil.move(path_to_folder, backup_folder_path)
        os.makedirs(path_to_folder, exist_ok=True)
    else:
        shutil.copytree(path_to_folder, backup_folder_path)
    return True


def is_file(path):
    """
    Checks if given path is a file or a directory
    Args:
        path(str): Path to check
    """
    filename, ext = os.path.splitext(path)
    if not ext:
        return False
    return True


def generate_id(base_path, type_of_file, server_ip, project_name):
    """
    Generates unique id for every unique string.

    Args: base_path : path at which the hierarchy of the files needs to fetched
          type_of_file: file/directory (Type of the fetched file)
          server_ip: ip of hdfs/nfs server
          project_name: name of the requested project

    Return: returns the hashed id generated combining nfs_server_ip,
            project_name, base_path, type
    """
    # use fstring
    unique_string = f'{server_ip}_{project_name}_{base_path}_{type_of_file}'
    hashed_id = hashlib.md5(unique_string.encode())
    hashed_id = hashed_id.hexdigest()
    return hashed_id


def current_time_in_xpr_format():
    """
    fetch current time as per the standard xpresso format
    """
    current_time = datetime.now().strftime("%c")
    return current_time
